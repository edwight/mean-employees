const mongoose = require('mongoose');
const { Schema } = mongoose;

const EmployerSchema = new Schema({
    name: { type:String, required:true },
    position: {type:String,required:true},
    office: {type:String,required:true},
    salary: {type:Number,requered:true}
})

module.exports = mongoose.model('employerSchema',EmployerSchema);
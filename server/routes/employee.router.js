const express = require('express');
const router = express.Router();
const emploCtrl = require('../controllers/employer.controller');

router.get('/', emploCtrl.getEmployee);
router.post('/', emploCtrl.createEmployee);
router.get('/:id', emploCtrl.getUniqueEmployee);
router.put('/:id', emploCtrl.editEmployee);
router.delete('/:id',emploCtrl.deleteEmployee);

module.exports = router;
const express = require('express');
const app = express();
const morgan = require('morgan');
var bodyParser = require('body-parser');
const { mongoose } = require('./database');

const routes = require('./routes/employee.router');
//settings 
app.set('post', process.env.POST || 3000);
app.use(bodyParser.urlencoded({ extended: false }))
 
//middleware 
app.set(morgan('dev'));
//app.set(express.json());
app.use(bodyParser.json())
//routes
app.use('/api/employees', routes);

app.listen(app.get('post'), ()=>{
    console.log(`serve funcionando en el puerto ${app.get('post')}`);
}); 
const Employer = require('../models/employer');
const employerCtrl = {};

employerCtrl.getEmployee = async function(req,res){
    const employee = await Employer.find();
    res.json(employee);
}
employerCtrl.createEmployee = async (req, res)=>{
    const employee = new Employer(req.body);
    await Employee.save();
    res.json({
        status:'employee saved'
    })
}
employerCtrl.getUniqueEmployee = async (req,res)=>{
    //let id = req.params.id;
    const employee = await Employer.findById(req.params.id);
    res.json(employee);
}
employerCtrl.editEmployee = (req,res)=>{
    res.json({
        status:'api edit employees'
    })
}
employerCtrl.deleteEmployee = (req,res)=>{
    res.json({
        status:'api edit employees'
    })
}
module.exports = employerCtrl;